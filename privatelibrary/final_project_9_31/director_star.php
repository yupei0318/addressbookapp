
<?php
?>

<html>
<head>
  <title>Another Simple PHP-MySQL Program</title>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
include('connectionData.txt');



$mysqli = new mysqli($server, $user, $pass, $dbname, $port);
if ($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: " .mysqli_connect_errno();
	exit();
	}


$query = "SELECT p1.name ".
         "FROM person p1 JOIN person p2 ".
         "WHERE p1.name = p2.name AND p1.occupation>p2.occupation ";


?>

<p>
The query:
<p>
<?php

print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php


/*create a prepared statement*/
if ($stmt = $mysqli->prepare($query)){
    /*bind parameters*/
	//$stmt->bind_param("ss",$manufacture,$state);
	/*execute it*/
	$stmt->execute();
	/*bind results*/
	
	$stmt->store_result();
	$stmt->bind_result($name);
	/*fetch the value*/
    while($stmt->fetch())
    {
	   echo "$name \n";
           echo "<br>";
	  // printf ("%s %s\n", $name, $total);
	}
	/*close statement*/
	$stmt->close();
}

/*close connection*/
$mysqli->close();
?>
<p>
<hr>

<p>
<a href="privateLibsearch.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>
	  
