
<?php
?>

<html>
<head>
  <title>Another Simple PHP-MySQL Program</title>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
include('connectionData.txt');
if (isset($_POST['state'])){
    $state = $_POST['state'];
}

if (isset($_POST['manufacturer'])){
    $manufacture = $_POST['manufacturer'];
}

$mysqli = new mysqli($server, $user, $pass, $dbname, $port);
if ($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: " .mysqli_connect_errno();
	exit();
	}


$query = "SELECT concat(fname, ' ', lname) as customer_name, concat('$', ifnull(SUM(t.price), 0)) as total ".
         "FROM customer c LEFT JOIN ".
         "(SELECT i.total_price as price, m.manu_name as manu_name, o.customer_num as customer_num ".
         "FROM items i JOIN orders o USING( order_num) JOIN manufact m USING (manu_code) ";


?>

<p>
The query:
<p>
<?php

$query1 = $query."WHERE m.manu_name = '".$manufacture."')t ON (c.customer_num=t.customer_num) ".
          "WHERE c.state = '".$state."' GROUP BY c.customer_num ORDER BY SUM(t.price);";
$query = $query."WHERE m.manu_name = ?)t ON (c.customer_num=t.customer_num) ".
		 "WHERE c.state = ? ".
		 "GROUP BY c.customer_num ".
		 "ORDER BY SUM(t.price);";

print $query1;
?>

<hr>
<p>
Result of query:
<p>

<?php


/*create a prepared statement*/
if ($stmt = $mysqli->prepare($query)){
    /*bind parameters*/
	$stmt->bind_param("ss",$manufacture,$state);
	/*execute it*/
	$stmt->execute();
	/*bind results*/
	
	$stmt->store_result();
	$stmt->bind_result($name, $total);
	/*fetch the value*/
    while($stmt->fetch())
    {
	   echo "$name $total\n";
           echo "<br>";
	  // printf ("%s %s\n", $name, $total);
	}
	/*close statement*/
	$stmt->close();
}

/*close connection*/
$mysqli->close();
?>
<p>
<hr>

<p>
<a href="privateLibsearch.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>
	  
