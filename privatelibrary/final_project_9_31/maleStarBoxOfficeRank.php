
<?php
?>

<html>
<head>
  <title>Another Simple PHP-MySQL Program</title>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
include('connectionData.txt');


$mysqli = new mysqli($server, $user, $pass, $dbname, $port);
if ($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: " .mysqli_connect_errno();
	exit();
	}


$query = "SELECT sb.star_name as actor, f.title, CONCAT('$',f.box_office_million) as box_office_million ".
         "FROM film f JOIN star_by sb USING (title) JOIN person p ON sb.star_name = p.name ".
         "WHERE p.gender = 'male' ORDER BY f.box_office_million DESC;";
         
?>

<p>
The query:
<p>
<?php

print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php


/*create a prepared statement*/
if ($stmt = $mysqli->prepare($query)){
    /*bind parameters*/
	//$stmt->bind_param("ss",$manufacture,$state);
	/*execute it*/
	$stmt->execute();
	/*bind results*/
	
	$stmt->store_result();
	$stmt->bind_result($name, $title, $box_office);
	/*fetch the value*/
    while($stmt->fetch())
    {
	   echo "$name, $title, $box_office\n";
           echo "<br>";
	  // printf ("%s %s\n", $name, $total);
	}
	/*close statement*/
	$stmt->close();
}

/*close connection*/
$mysqli->close();
?>
<p>
<hr>

<p>
<a href="maleStarBoxOfficeRank.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>
	  
