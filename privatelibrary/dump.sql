-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: ix-trusty    Database: privateLib
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `privateLib`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `privateLib` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `privateLib`;

--
-- Table structure for table `author_by`
--

DROP TABLE IF EXISTS `author_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author_by` (
  `b_name` char(30) NOT NULL,
  `author_name` char(45) NOT NULL,
  PRIMARY KEY (`b_name`,`author_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_by`
--

LOCK TABLES `author_by` WRITE;
/*!40000 ALTER TABLE `author_by` DISABLE KEYS */;
INSERT INTO `author_by` VALUES ('A Dream of Death','Harrison Drake'),('Child\'s Play','Louisa May Alcott'),('Dune','Frank Herbert'),('Dune','Kyle Gates'),('Ender\'s Game','Orson Card'),('Gone With the Wind','Margaret Mitchell'),('Leaves of Grass','Walt Whitman'),('Little Women','Louisa May Alcott'),('Odyssey','Homer'),('Paradise Lost','John Milton'),('Pride and Prejudice','Ian Edginton'),('Red Carpet','Kyle Eugene'),('Rush','Orson Card'),('The Girl with the Dr','Stieg Larsson'),('The Great Gatsby','F. Scott Fitzgerald'),('The Hobbit','J. R. R Tolkien'),('The Lord of Rings','Brian Sibley'),('The Sherlock Holmes','Arthur Conan Doyle'),('To Kill a Mockingbir','Harper Lee'),('Wheat','Louisa May Alcott');
/*!40000 ALTER TABLE `author_by` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `title` char(40) NOT NULL,
  `genre` char(45) DEFAULT NULL,
  `status` char(45) NOT NULL,
  `page_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES ('A Dream of Death','Detective','out',230),('Dune','Science fiction','missing',233),('Ender\'s Game','Science fiction','in',120),('Gone With the Wind','Novel','out',221),('Leaves of Grass','Poetry','in',130),('Little Women','Novel','in',455),('Odyssey','Poetry','sold',455),('Paradise Lost','Poetry','in',341),('Pride and Prejudice','Novel','out',231),('Red Carpet','Novel','missing',344),('The Girl with the Dr','Mystery','sold',230),('The Great Gatsby','Novel','out',211),('The Hobbit','Novel','missing',120),('The Lord of Rings','Novel','in',134),('The Sherlock Holmes','Detective','in',145),('To Kill a Mockingbird','Novel','in',322);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director_by`
--

DROP TABLE IF EXISTS `director_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `director_by` (
  `title` char(25) NOT NULL,
  `dir_name` char(25) NOT NULL,
  PRIMARY KEY (`title`,`dir_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director_by`
--

LOCK TABLES `director_by` WRITE;
/*!40000 ALTER TABLE `director_by` DISABLE KEYS */;
INSERT INTO `director_by` VALUES ('Beautiful Life','Guy Ritchie'),('Despicable Me','Pierre Coffin'),('Ice Age','Chris Wedge'),('Kung Fu Panda','Melissa Cobb'),('Labor Day','Jason Reitman'),('Mr. & Mrs. Smith','Doug Liman'),('Original Sin','Michael Cristofer'),('Ring','Guy Ritchie'),('Silver Linings Playbook','David O. Russell'),('The Amazing Spider-Man','Marc Webb'),('The Exorcist','William Friedkin'),('The Great Gatsby','Baz Luhmann'),('The Hobbit','Peter Jackson'),('The Rush','Ian McKellen'),('The Spring','Bob Cooper'),('Titanic','James Cameron'),('Troy','Antonio Banderas');
/*!40000 ALTER TABLE `director_by` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `film` (
  `title` char(45) NOT NULL,
  `genre` char(45) DEFAULT NULL,
  `director_name` char(45) NOT NULL,
  `box_office_million` int(11) DEFAULT NULL,
  `company` char(45) DEFAULT NULL,
  PRIMARY KEY (`title`,`director_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film`
--

LOCK TABLES `film` WRITE;
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` VALUES ('Beautiful Life','Mystery','Guy Ritchie',376,'Warner Bros'),('Despicable Me','Anime','Pierre Coffin',543,'IIIumination Entertainment'),('Ice Age','Anime','Chris Wedge',383,'20th Century Fox'),('Kung Fu Panda','Anime','Melissa Cobb',631,'DreamWorks Animation'),('Labor Day','Drama','Jason Reitman',19,'Paramount Pictures'),('Mr. & Mrs. Smith','Action','Doug Liman',478,'20th Century Fox'),('Original Sin','Romance','Michael Cristofer',36,'Hyde Park Entertainment'),('Ring','Horror','Guy Ritchie',216,'Warner Bros'),('Sherlock Holmes','Detective','Guy Ritchie',542,'Warner Bros'),('Silver Linings Playbook','Comedy','David O. Russell',236,'The Weinstein Company'),('The Amazing Spider-Man','Fantasy','Marc Webb',757,'Columbia Pictures'),('The Exorcist','Horror','William Friedkin',441,'William Peter Blatty'),('The Great Gatsby','Romance','Baz Luhmann',351,'Warner Bros'),('The Hobbit','Fantasy','Peter Jackson',1975,'New Line Cinema'),('The Spring','Romance','Bob Cooper',456,'20th Century Fox'),('Titanic','Romance','James Cameron',2187,'20th Century Fox'),('Troy','Romance','Wolfgang Petersen',497,'Helena Productions');
/*!40000 ALTER TABLE `film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `order_num` bigint(6) DEFAULT NULL,
  `title` char(40) NOT NULL,
  `genre` varchar(45) DEFAULT NULL,
  `status` char(15) NOT NULL,
  `category` char(15) NOT NULL,
  PRIMARY KEY (`title`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1020,'A Dream of Death','Detective','out','book'),(1040,'Arrow','Science Fiction','sold','tvshow'),(1041,'Desperate Housewives','Mystery','in','tvshow'),(1001,'Despicable Me','Anime','missing','film'),(1021,'Dune','Science fiction','missing','book'),(1022,'Ender\'s Game','Science fiction','in','book'),(1042,'Friends','Sitcom','missing','tvshow'),(1043,'Game of Thrones','Fantasy','in','tvshow'),(1033,'Gone With the Wind','Novel','out','book'),(1044,'Grey\'s Anatomy','Medical Drama','out','tvshow'),(1045,'How I Met Your Mother','Sitcom','in','tvshow'),(1002,'Ice Age','Anime','in','film'),(1003,'Kung Fu Panda','Anime','in','film'),(1004,'Labor Day','Drama','out','film'),(1024,'Leaves of Grass','Poetry','in','book'),(1023,'Little Women','Novel','in','book'),(1005,'Mr. & Mrs. Smith','Action','sold','film'),(1046,'Nikita','Action','in','tvshow'),(1025,'Odyssey','Poetry','sold','book'),(1006,'Original Sin','Romance','in','film'),(1027,'Paradise Lost','Poetry','in stock','book'),(1026,'Pride and Prejudice','Novel','checked out','book'),(1007,'Sherlock Holmes','Detective','in','film'),(1008,'Silver Linings Playbook','Comedy','out','film'),(1047,'Supernatural','Horror','out','tvshow'),(1009,'The Amazing Spider-Man','Fantasy','in','film'),(1048,'The Big Bang Theory','Sitcom','in','tvshow'),(1049,'The Edge','Comedy','missing','tvshow'),(1010,'The Exorcist','Horror','in','film'),(1028,'The Girl with the Lion','Mystery','sold','book'),(1034,'The Great Gatsby','Novel','out','book'),(1011,'The Great Gatsby','Romance','in','film'),(1029,'The Hobbit','Novel','missing','book'),(1012,'The Hobbit','Fantasy','out','film'),(1005,'The Lord of Rings','Novel','in','book'),(1050,'The Office','Sitcom','in','tvshow'),(1031,'The Sherlock Holmes','Detective','in','book'),(1013,'Titanic','Romance','missing','film'),(1014,'To Kill a Mockingbird','Novel','in','book'),(1014,'Troy','Romance','in','film');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_num` bigint(6) NOT NULL,
  `order_date` date DEFAULT NULL,
  `retailer` char(25) DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`order_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1001,'2001-10-01','Amazon',33.69),(1002,'2010-03-12','Ebay',49.95),(1003,'2008-02-26','Amazon',23.69),(1004,'2011-05-16','Smith Family',23.56),(1005,'2014-04-29','Barnes & Noble',35.68),(1006,'2013-09-17','Powell\'s',33.29),(1007,'2014-08-23','Smith Family',58.98),(1008,'2010-03-20','Amazon',60.39),(1009,'2007-07-18','Amazon',12.39),(1010,'2003-11-24','Barnes & Noble',19.99),(1011,'2006-10-08','Barnes & Noble',39.09),(1012,'2004-01-31','Barnes & Noble',46.37),(1013,'2005-03-12','Ebay',40.28),(1014,'2008-09-10','Barnes & Noble',19.99),(1020,'2003-10-15','Ebay',78.99),(1021,'2005-07-19','Amazon',58.88),(1023,'2013-12-09','Amazon',45.99),(1024,'2014-03-24','Ebay',43.99),(1025,'2010-02-04','Barnes & Noble',101.98),(1026,'2009-03-05','Amazon',29.99),(1027,'2008-05-30','Ebay',35.98),(1028,'2004-10-02','Barnes & Noble',56.79),(1029,'2005-07-25','Ebay',34.99),(1031,'2012-04-04','Powell\'s',29.59),(1033,'1989-02-04','Powell\'s',45.66),(1034,'1992-04-03','Ebay',45.69),(1041,'2011-12-14','Amazon',24.99),(1042,'2007-03-12','Amazon',25.98),(1044,'2012-09-07','Amazon',130.99),(1045,'2013-12-12','Amazon',59.99),(1047,'2013-10-30','Amazon',69.98),(1048,'2014-07-04','Smith Family',79.99),(1050,'2014-08-12','Smith Family',37.70);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `name` char(20) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `country` char(10) DEFAULT NULL,
  `spouse_name` char(45) DEFAULT NULL,
  `occupation` char(45) NOT NULL,
  `gender` char(45) DEFAULT NULL,
  PRIMARY KEY (`name`,`occupation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES ('Angelina Jolie',45,'US','Brad Pitt','star','Female'),('Antonio Banderas',34,'US',NULL,'director','Male'),('Antonio Banderas',34,'US',NULL,'star','Male'),('Arthur Conan Doyle',25,'Spain','Penny Doyle','author','Male'),('Baz Luhmann',37,NULL,'Bethy Chris','director','Male'),('Bob Cooper',57,NULL,'Melissa Cobb','director','Male'),('Brad Pitt',46,'US','Angelina Jolie','star','Male'),('Bradley Cooper',34,'US',NULL,'star','Male'),('Brian Sibley',56,'UK','Mellody Sibley','author','Male'),('Chris Wedge	',46,'US',NULL,'director','Male'),('David O. Russell',50,NULL,NULL,'director','Male'),('Doug Liman',45,NULL,NULL,'director','Male'),('Ellen Burstyn',48,NULL,NULL,'star','Female'),('Eric Bana',51,NULL,NULL,'star','Male'),('F. Scott Fitzgerald',18,'Spain','','author','Male'),('Frank Herbert',34,'US','Tiffny Herbert','author','Male'),('Guy Ritchie',57,NULL,NULL,'director','Male'),('Harper Lee',23,'Canada','Jeniffer Lee','author','Male'),('Harrison Drake',67,'US','Tina Hilton','author','Male'),('Homer',54,'Austrilia','','author','Male'),('Ian Edginton',45,'US','Kate Webb','author','Male'),('Ian McKellen',26,NULL,NULL,'director','Male'),('Ian McKellen',26,NULL,NULL,'star','Male'),('J. R. R Tolkien',37,'UK','Blair Yong','author','Male'),('Jack Black',33,NULL,NULL,'star','Male'),('James Cameron',31,NULL,'Mary Patterson','director','Male'),('Jason Reitman',38,NULL,'Ellen Bush','star','Male'),('Jeniffer Lawrance',21,'US',NULL,'star','Female'),('John Milton',78,'Germany','Bethy Dodge','author','Male'),('Josh Brolin',26,NULL,NULL,'star','Male'),('Jude Law',25,NULL,NULL,'star','Female'),('Kate Winslet',45,NULL,'Leonardo DiCaprio','star','Female'),('Kyle Eugene',55,'US','Orson Card','author','Female'),('Leonardo DiCaprio',35,NULL,'Kate Winslet','star','Male'),('Louisa May Alcott',69,'France','Jack Cliton','author','Female'),('Marc Webb',56,NULL,NULL,'director','Male'),('Margaret Mitchell',54,'France','Kristina Mitchell','author','Female'),('Martin Freeman',67,NULL,NULL,'star','Male'),('Melissa Cobb',54,NULL,'Bob Cooper','director','Female'),('Michael Cristofer',63,NULL,NULL,'director','Male'),('Orson Card',57,'US','Kyle Eugene','author','Male'),('Perrie Cash',32,NULL,NULL,'star','Female'),('Peter Jackson',56,NULL,NULL,'director','Male'),('Pierre Coffin',45,NULL,NULL,'director','Male'),('Ray Romano	',67,NULL,'Bethy Chris','star','Male'),('Robert Downey',36,NULL,'Kyle Smith','star','Male'),('Steve Carell',44,NULL,NULL,'star','Male'),('Stieg Larsson',34,'UK','Regina White','author','Male'),('Tobey Maguire',45,NULL,NULL,'star','Female'),('Walt Whitman',37,'Russia','Jet Feyer','author','Male'),('William Friedkin',37,NULL,NULL,'director','Male'),('Wolfgang Petersen',35,NULL,NULL,'director','Male');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `star_by`
--

DROP TABLE IF EXISTS `star_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `star_by` (
  `title` char(25) NOT NULL,
  `star_name` char(25) NOT NULL,
  `category` char(45) NOT NULL,
  PRIMARY KEY (`title`,`star_name`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `star_by`
--

LOCK TABLES `star_by` WRITE;
/*!40000 ALTER TABLE `star_by` DISABLE KEYS */;
INSERT INTO `star_by` VALUES ('Arrow','Greg Berlanti','tvshow'),('Beautiful Life','Josh Witt','flim'),('Beautiful Life','Ryan Chan','flim'),('Desperate Housewives','Felicity Huffman','tvshow'),('Desperate Housewives','Termi Hatcher','tvshow'),('Despicable Me','Steve Carell','film'),('Friends','Courteney Cox','tvshow'),('Friends','Jennifer Aniston','tvshow'),('Game of Thrones','Kit Harington','tvshow'),('Game of Thrones','Peter Dinklage','tvshow'),('Grey\'s Anatomy','Ellen Pompeo','tvshow'),('Grey\'s Anatomy','Sandra Oh','tvshow'),('How I Met Your Mother','Jason Segel','tvshow'),('How I Met Your Mother','Josh Radnor','tvshow'),('Ice Age','Ray Romano','film'),('Kung Fu Panda','Jack Black','film'),('Labor Day','Josh Brolin','film'),('Labor Day','Kate Winslet','film'),('Mr. & Mrs. Smith','Angelina Jolie','film'),('Mr. & Mrs. Smith','Brad Pitt','film'),('Nikita','Maggie Q','tvshow'),('Nikita','Shane West','tvshow'),('Original Sin','Angelina Jolie','film'),('Original Sin','Antonio Banderas','film'),('Ring','Elizabeth Brown','flim'),('Ring','Leonardo DiCaprio','flim'),('Sherlock Holmes','Jude Law','film'),('Sherlock Holmes','Robert Downey','film'),('Silver Linings Playbook','Bradley Cooper','film'),('Silver Linings Playbook','Jenifer Lawrence','film'),('Supernatural','Jared Padalecki','tvshow'),('The Amazing Spider-Man','Andrew Garfield','film'),('The Big Bang Theory','Jensen Ackles','tvshow'),('The Big Bang Theory','Jim Parsons','tvshow'),('The Big Bang Theory','Johnny Galecki','tvshow'),('The Edge','Brad Pitt','film'),('The Edge','Jeniffer Aniston','tvshow'),('The Edge','Peter Baldwin','tvshow'),('The Exorcist','Ellen Burstyn','film'),('The Exorcist','Max von Sydow','film'),('The Girl with the Lion','Jack Black','film'),('The Great Gatsby','Leonardo DiCaprio','film'),('The Great Gatsby','Tobey Maguire','film'),('The Hobbit','Ian McKellen','film'),('The Hobbit','Kate Winslet','film'),('The Hobbit','Martin Free','film'),('The Office','Shane West','tvshow'),('The Office','Steve Carell','tvshow'),('The Spring','Johnny Galecki','film'),('The Spring','Leonardo DiCaprio','flim'),('The Spring','Max von Sydow','flim'),('The Spring','Steve Carell','flim'),('Titanic','Kate Winslet','film'),('Titanic','Leonardo DiCaprio','film'),('Titanic','Perrie Cash','flim'),('Troy','Brad Pitt','film'),('Troy','Eric Bana','film'),('Troy','Sandra Oh','film');
/*!40000 ALTER TABLE `star_by` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tv_show`
--

DROP TABLE IF EXISTS `tv_show`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tv_show` (
  `title` char(45) NOT NULL,
  `genre` char(45) DEFAULT NULL,
  `status` char(45) NOT NULL,
  `season_num` char(45) NOT NULL,
  `company` char(45) DEFAULT NULL,
  `channel` char(45) DEFAULT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tv_show`
--

LOCK TABLES `tv_show` WRITE;
/*!40000 ALTER TABLE `tv_show` DISABLE KEYS */;
INSERT INTO `tv_show` VALUES ('Arrow','Science Fiction','in','3','Warner Bros','The CW'),('Desperate Housewives','Mystery','out','8','ABC Studios','ABC'),('Friends','Sitcom','in','10','Warner Bros','NBC'),('Game of Thrones','Fantasy','in','4','Warner Bros','HBO'),('Grey\'s Anatomy','Medical Drama','in','11','ABC Studios','ABC'),('How I Met Your Mother','Sitcom','missing','9','20th Century Fox','CBS'),('Nikita','Action','in','4','Warner Bros','The CW'),('Supernatural','Horror','sold','10','Warner Bros','The CW'),('The Big Bang Theory','Sitcom','in','8','CBS','CBS'),('The Edge','Comedy','out','1','20th Century Fox','FOX'),('The Office','Sitcom','missing','9','NBC','NBC');
/*!40000 ALTER TABLE `tv_show` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-10  3:31:37
