
<?php
?>

<html>
<head>
  <title>Another Simple PHP-MySQL Program</title>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
include('connectionData.txt');

$mysqli = new mysqli($server, $user, $pass, $dbname, $port);
if ($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: " .mysqli_connect_errno();
	exit();
	}


$query = "SELECT DISTINCT top.star_name, title, CONCAT('$', f.box_office_million ) as box_office_million ".
         "FROM star_by sb1 JOIN film f USING (title) JOIN ".
         "(SELECT star_name, COUNT(title) as number FROM star_by sb2 GROUP BY sb2.star_name ORDER BY number DESC LIMIT 1) AS top ".
         "WHERE sb1.star_name LIKE top.star_name ".
		 "ORDER BY f.box_office_million DESC;";


?>

<p>
The query:
<p>
<?php

print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php


/*create a prepared statement*/
if ($stmt = $mysqli->prepare($query)){
    /*bind parameters*/
	/*execute it*/
	$stmt->execute();
	/*bind results*/
	
	$stmt->store_result();
	$stmt->bind_result($name, $title, $boxoffice);
	/*fetch the value*/
    while($stmt->fetch())
    {
	   echo "$name, $title, $boxoffice\n";
           echo "<br>";
	  // printf ("%s %s\n", $name, $total);
	}
	/*close statement*/
	$stmt->close();
}

/*close connection*/
$mysqli->close();
?>
<p>
<hr>

<p>
<a href="mostProductiveStar.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>
	  
