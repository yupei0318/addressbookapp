import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class Contacts {
	
		 
	    //private String id;
	    private String firstName;
	    private String lastName;
	    private String zipCode;
	    private String address;
	    private String address2;
	    private String city;
	    private String state;
	    private String phone;
	    private String cell;
	    private String email;
	
	    public Contacts() {
	 
	    }


	   // public Contacts(String id) {//*, String firstName,String lastName*/) 
	   
	    	//this.id = id;
	        //this.firstName = firstName;
	       // this.lastName = lastName;
	   // }
 
 /*
	    public String getId() {
	        return id;
	    }
	    
	 */  
	 /*
	    public void setId(String id) {
	        this.id = id;
	    }

      */
	    public String getFirstName() {
	        return firstName;
	    }
	 
	    public void setFirstName(String firstName) {
	        this.firstName = firstName;
	    }
	 
	    public String getLastName() {
	        return lastName;
	    }
	 
	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	    
	    public String getZipcode() {
	        return zipCode;
	    }
	    public void setZipcode(String zipCode) {
	        this.zipCode = zipCode;
	    }
	    
	    public String getAddress() {
	        return address;
	    }
	    public void setAddress(String address) {
	        this.address = address;
	    }
	    
	    
	    public String getAddress2() {
	        return address2;
	    }
	    public void setAddress2(String address2) {
	        this.address2 = address2;
	    }
	    
	    public String getCity() {
	        return city;
	    }
	    public void setCity(String city) {
	        this.city = city;
	    }
	    
	
	    public String getState() {
	        return state;
	    }
	    public void setState(String state) {
	        this.state = state;
	    }
	    
	    public String getPhone() {
	        return phone;
	    }
	    public void setPhone(String phone) {
	        this.phone = phone;
	    }
	
	    public String getCell() {
	        return cell;
	    }
	    public void setCell(String cell) {
	        this.cell = cell;
	    }
	    
	    public String getEmail() {
	        return email;
	    }
	    public void setEmail(String email) {
	        this.email = email;
	    }
	    
	    
	    	
    	public String[] toStrings() {
            return new String[]{ firstName, lastName, address, address2, city, state, zipCode, phone, email};
        }

        public static String[][] toStringsMatrix(ArrayList<Contacts> contactList) {
            int rows = contactList.size();
            String[] first = contactList.get(0).toStrings();
            int columns = first.length;
            String[][] table = new String[rows][columns];
            table[0] = first;
            for (int k = 1; k < rows; k++) {
                table[k] = contactList.get(k).toStrings();
            }
            return table;
        }
    


		
}
	

