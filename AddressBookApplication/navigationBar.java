
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;



@SuppressWarnings("serial")
public class navigationBar extends JMenuBar {
	
	public navigationBar(JFrame mainFrame) {
		JMenuBar menuBar = new JMenuBar();
		mainFrame.setJMenuBar(menuBar);
		
		//File Menu
		JMenu menu = new JMenu("File");
		menuBar.add(menu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("New Address Book");
		menu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	
		   
		    		EventQueue.invokeLater(new Runnable() {
		    			public void run() {
		    				try {
		    					gui window = new gui();
		    					window.mainFrame.setVisible(true);
		    					
		    				} catch (Exception e) {
		    					e.printStackTrace();
		    				}
		    			}
		    		});
		    		
		    	}
			
		});
		
		
		//End file menu
		
		//Help Menu
		JMenu mnNewMenu = new JMenu("Help");
		menuBar.add(mnNewMenu);
		
	}
}
