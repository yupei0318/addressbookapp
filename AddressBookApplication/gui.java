import java.awt.GridLayout;
import java.awt.Panel;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;


public class gui {
	
	public JFrame mainFrame;
	public JTabbedPane tabbedPane;

	
	public gui() {
		initialize();
		

	}

	private void initialize() {
		mainFrame = new JFrame();
		mainFrame.setBounds(100, 100, 900, 600);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		Panel panel = new Panel();
		mainFrame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		tabbedPane.setBounds(10, 11, 858, 500);
		panel.add(tabbedPane);
		
		addressListPane addressList = new addressListPane(tabbedPane);
		
		navigationBar menuBar = new navigationBar(mainFrame);
		
	}
	

}
