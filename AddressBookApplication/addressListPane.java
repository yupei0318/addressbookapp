
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;




@SuppressWarnings("serial")
public class addressListPane extends JPanel {
	
	private JList<String> list;
	private ArrayList<String> columns = new ArrayList<String>();
	private ArrayList<Contacts> cList = new ArrayList<Contacts>();
	public DefaultTableModel tableModel;
	private JTable table;
	private JLabel contactName;
	private JLabel contactAddress;
	private JLabel contactAddress2;
	private JLabel contactZip;
	private JLabel contactPhone;
	private JLabel contactCell;
	private JLabel contactEmail;
	private JLabel contactCity;
	private JLabel contactState;
	//private TableModel tableModel;
	private ArrayList<JTextField> textFields;
	private JPanel addressBook_mainPanel = new JPanel();
	private JFrame frame = new JFrame();;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_export;
	private int counter;
	private String fileName;
	private int selectedRow;
	private boolean firstTime = true;

	public addressListPane(JTabbedPane tabbedPane) {
	    columns.add("First Name");
	    columns.add("Last Name");
	    columns.add("Address");
	    columns.add("Address 2");
	    columns.add("City");
	    columns.add("State");
	    columns.add("Zip Code");
	    columns.add("Phone");
	    columns.add("Email");
		
		addressBook_mainPanel.setBorder(null);
		tabbedPane.addTab("Address Book", null, addressBook_mainPanel, null);
		addressBook_mainPanel.setLayout(null);
		
		JLabel lblContacts = new JLabel("Contacts");
		lblContacts.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContacts.setBounds(10, 11, 82, 14);
		addressBook_mainPanel.add(lblContacts);
		
		JLabel lblContactInformation = new JLabel("Contact Information");
		lblContactInformation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContactInformation.setBounds(10, 261, 124, 14);
		addressBook_mainPanel.add(lblContactInformation);
		
		//buildTable();
		
		/*CONTACT INFO PANE*/
		JPanel contactInfo_panel = new JPanel();
		contactInfo_panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contactInfo_panel.setBounds(10, 286, 833, 175);
		addressBook_mainPanel.add(contactInfo_panel);
		contactInfo_panel.setLayout(null);
		
		/*EDIT CONTACT*/
		JButton editButton = new JButton("Edit Contact");
		editButton.setBounds(10, 141, 110, 23);
		contactInfo_panel.add(editButton);
		
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				editContact();
				
				}
		});
		
		/*DELETE CONTACT*/
		
		JButton deleteButton = new JButton("Delete Contact");
		deleteButton.setBounds(127, 141, 120, 23);
		contactInfo_panel.add(deleteButton);
		
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table != null){
					int selectedRow = table.convertRowIndexToModel(table.getSelectedRow());
					cList.remove(selectedRow);
					deleteRefresh(cList);
					
				}
				else{
					JOptionPane.showMessageDialog(null, "The address book is empty!");
				}
			}
			
		});
		
		
		/*EXPORT CONTACT TO TSV FILE*/
		JButton exportButton = new JButton("Export");
		exportButton.setBounds(270, 141, 100, 23);
		contactInfo_panel.add(exportButton);
		exportButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportAddressBook();
			}
		});
		
		/*SORT CONTACTS BY NAMES*/
		JButton sortNameButton = new JButton("Sort by Name");
		sortNameButton.setBounds(680, 15, 130, 23);
		contactInfo_panel.add(sortNameButton);
		sortNameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sortName(cList);
				refresh(cList);
			}
		});
		
		/*SORT CONTACTS BY ZIPCODE*/
		JButton sortZipButton = new JButton("Sort by Zipcode");
		sortZipButton.setBounds(680, 45, 130, 23);
		contactInfo_panel.add(sortZipButton);
		sortZipButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sortZipcode(cList);
				refresh(cList);
				
			}
		});
		
		/*IMPORT CONTACTS FROM A TSV FILE*/
		JButton importButton = new JButton("Import");
		importButton.setBounds(590, 141, 100, 23);
		contactInfo_panel.add(importButton);
		importButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openAddressBook();
				
			}
		});
		
		
		
		/*SAVE TO A DEFAULT NEW FILE IN CURRENT WORKING DIRECTORY WHEN FIRST CLICK THIS BUTTON;
		 * FURTHER CLICK OF THIS BUTTON WILL UPDATE CHANGES TO THIS DEFAULT FILE*/
		JButton saveFileButton = new JButton("Save");
		saveFileButton.setBounds(390, 141, 90, 23);
		contactInfo_panel.add(saveFileButton);
		saveFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//if (textField_1 != null){
				if (counter==0){
					saveFile(counter);
					
				}
				else{
					File file = new File(System.getProperty("user.dir"), fileName);
					overwriteFile(cList, file);
					return;
				}
				}
		
		});
		
		/*SAVE AS A NEW TSV FILE IN CURRENT DIRECTORY*/
		JButton saveAsButton = new JButton("Save As");
		saveAsButton.setBounds(490, 141, 90, 23);
		contactInfo_panel.add(saveAsButton);
		saveAsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				chooseAddressBook();
			}
		});
		
		
		/*EXIT CURRENT ADDRESS BOOK*/
		JButton exitButton = new JButton("EXIT");
		exitButton.setBounds(700, 141, 100, 23);
		contactInfo_panel.add(exitButton);
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    System.exit(0);
			        }
			    });
			
		/*IMAGE*/
		JLabel contactIcon = new JLabel("");
		contactIcon.setIcon(new ImageIcon("src/res/icon-user-default.png"));
		contactIcon.setBounds(10, 11, 120, 120);
		contactInfo_panel.add(contactIcon);
		
		/*NAME FIELDS*/
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 14, 76, 16);
		contactInfo_panel.add(lblName);
		
		contactName = new JLabel("TEST NAME");
		contactName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactName.setBounds(96, 13, 252, 17);
		contactInfo_panel.add(contactName);
		
		/*ADDRESS FIELD 1*/
		JLabel lblAddress_1 = new JLabel("Address:");
		lblAddress_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress_1.setBounds(372, 12, 76, 18);
		contactInfo_panel.add(lblAddress_1);
		
		contactAddress = new JLabel("3333 TEST ADDRESS");
		contactAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactAddress.setBounds(458, 11, 252, 19);
		contactInfo_panel.add(contactAddress);
		
		/*ADDRESS FIELD 2*/
		JLabel lblAddress_2 = new JLabel("Address 2: ");
		lblAddress_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress_2.setBounds(10, 41, 76, 14);
		contactInfo_panel.add(lblAddress_2);
		
		contactAddress2 = new JLabel("4444 TEST ADDRESS");
		contactAddress2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactAddress2.setBounds(96, 40, 252, 17);
		contactInfo_panel.add(contactAddress2);
		
		/*ZIP CODE*/
		JLabel lblZipcode = new JLabel("Zip Code: ");
		lblZipcode.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblZipcode.setBounds(372, 41, 76, 16);
		contactInfo_panel.add(lblZipcode);
		
		contactZip = new JLabel("97401-2045");
		contactZip.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactZip.setBounds(458, 40, 138, 17);
		contactInfo_panel.add(contactZip);
		
		/*PHONE NUMBER*/
		JLabel lblPhone = new JLabel("Phone #:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPhone.setBounds(10, 91, 76, 14);
		contactInfo_panel.add(lblPhone);
		
		contactPhone = new JLabel("555-555-5555");
		contactPhone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactPhone.setBounds(96, 91, 138, 14);
		contactInfo_panel.add(contactPhone);
		
		/*CELL PHONE NUMBER*/
		JLabel lblCell = new JLabel("Cell #:");
		lblCell.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCell.setBounds(372, 89, 76, 16);
		contactInfo_panel.add(lblCell);
		
		contactCell = new JLabel("555-555-5555");
		contactCell.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactCell.setBounds(458, 89, 138, 16);
		contactInfo_panel.add(contactCell);
		
		/*EMAIL ADDRESS*/
		JLabel lblEmail = new JLabel("Email: ");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(10, 116, 76, 18);
		contactInfo_panel.add(lblEmail);
		
		contactEmail = new JLabel("example@example.com");
		contactEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactEmail.setBounds(96, 116, 252, 18);
		contactInfo_panel.add(contactEmail);
		
		/*CITY*/
		JLabel lblCity = new JLabel("City:");
		lblCity.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCity.setBounds(10, 66, 76, 14);
		contactInfo_panel.add(lblCity);
		
		contactCity = new JLabel("TEST CITY");
		contactCity.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactCity.setBounds(96, 66, 138, 14);
		contactInfo_panel.add(contactCity);
		
		/*STATE*/
		JLabel lblState = new JLabel("State:");
		lblState.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblState.setBounds(370, 68, 46, 14);
		contactInfo_panel.add(lblState);
		
		contactState = new JLabel("AA");
		contactState.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contactState.setBounds(458, 68, 22, 14);
		contactInfo_panel.add(contactState);
		
		JButton btnAddContact = new JButton("ADD CONTACT");
		btnAddContact.setBounds(691, 7, 152, 23);
		addressBook_mainPanel.add(btnAddContact);
		
		btnAddContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addContact();
			}
		});
	}
		
	
	
	@SuppressWarnings("static-access")
	public void addContact() {
		textFields = new ArrayList<JTextField>();
		//Main.cList
		final JFrame frame = new JFrame();
		frame.setBounds(100, 100, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFirstName.setBounds(10, 11, 80, 17);
		frame.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLastName.setBounds(10, 36, 80, 17);
		frame.getContentPane().add(lblLastName);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress.setBounds(10, 61, 80, 17);
		frame.getContentPane().add(lblAddress);
		
		JLabel lblAddress_1 = new JLabel("Address 2:");
		lblAddress_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress_1.setBounds(10, 86, 80, 17);
		frame.getContentPane().add(lblAddress_1);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCity.setBounds(10, 111, 80, 17);
		frame.getContentPane().add(lblCity);
		
		JLabel lblState = new JLabel("State:");
		lblState.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblState.setBounds(10, 139, 80, 17);
		frame.getContentPane().add(lblState);
		
		JLabel lblZipcode = new JLabel("Zipcode:");
		lblZipcode.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblZipcode.setBounds(10, 167, 80, 17);
		frame.getContentPane().add(lblZipcode);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPhone.setBounds(10, 195, 80, 17);
		frame.getContentPane().add(lblPhone);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(10, 223, 80, 14);
		frame.getContentPane().add(lblEmail);
		
		final JTextField firstNameField = new JTextField();
		firstNameField.setBounds(100, 11, 230, 20);
		frame.getContentPane().add(firstNameField);
		firstNameField.setColumns(10);
		textFields.add(firstNameField);
		
		final JTextField lastNameField = new JTextField();
		lastNameField.setBounds(100, 36, 230, 20);
		frame.getContentPane().add(lastNameField);
		lastNameField.setColumns(10);
		textFields.add(lastNameField);
		
		final JTextField addressField = new JTextField();
		addressField.setBounds(100, 61, 230, 20);
		frame.getContentPane().add(addressField);
		addressField.setColumns(10);
		textFields.add(addressField);
		
		final JTextField address2Field = new JTextField();
		address2Field.setBounds(100, 86, 230, 20);
		frame.getContentPane().add(address2Field);
		address2Field.setColumns(10);
		textFields.add(address2Field);
		
		final JTextField cityField = new JTextField();
		cityField.setBounds(100, 111, 230, 20);
		frame.getContentPane().add(cityField);
		cityField.setColumns(10);
		textFields.add(cityField);
		
		final JTextField stateField = new JTextField();
		stateField.setBounds(100, 139, 230, 20);
		frame.getContentPane().add(stateField);
		stateField.setColumns(10);
		textFields.add(stateField);
		
		final JTextField zipcodeField = new JTextField();
		zipcodeField.setBounds(100, 167, 230, 20);
		frame.getContentPane().add(zipcodeField);
		zipcodeField.setColumns(10);
		textFields.add(zipcodeField);
		
		final JTextField phoneField = new JTextField();
		phoneField.setBounds(100, 195, 230, 20);
		frame.getContentPane().add(phoneField);
		phoneField.setColumns(10);
		textFields.add(phoneField);
		
		final JTextField emailField = new JTextField();
		emailField.setBounds(100, 222, 230, 20);
		frame.getContentPane().add(emailField);
		emailField.setColumns(10);
		textFields.add(emailField);
		
		JButton btnSaveContact = new JButton("SAVE CONTACT");
		btnSaveContact.setBounds(371, 221, 178, 23);
		frame.getContentPane().add(btnSaveContact);
		
		btnSaveContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i < textFields.size(); i++) {
					if(textFields.get(i).getText().equals("")) {
						textFields.get(i).setText("n/a");
					}
				}
				//Name can only be letters 
				Contacts contact = new Contacts();
				
				if (!textFields.get(0).getText().equals("n/a")){
					if (!textFields.get(0).getText().matches("[a-zA-Z]+")){
						JOptionPane.showMessageDialog(null, "Please enter a valid first name!");}
					}
					else{
						contact.setFirstName(firstNameField.getText());
				}
			
				if (!textFields.get(1).getText().equals("n/a")){
					if (!textFields.get(1).getText().matches("[a-zA-Z]+")){
						JOptionPane.showMessageDialog(null, "Please enter a valid Last name!");}
					}
					else{
						contact.setLastName(lastNameField.getText());
				}
			
				
				//Check for zip code. It only accepts 00000-(0000) format.
				
				if (!textFields.get(6).getText().equals("n/a")){
					if (!textFields.get(6).getText().matches("^\\d{5}(-\\d{4})?$")){
						JOptionPane.showMessageDialog(null, "Please enter a valid zip code in format 00000-(0000)!");}
					}
					else{
						contact.setZipcode(zipcodeField.getText());
				}
				
					
				
				contact.setAddress( addressField.getText());
				contact.setAddress2( address2Field.getText());
				contact.setPhone(phoneField.getText());
				contact.setCity(cityField.getText());
				contact.setState(stateField.getText());
				contact.setEmail(emailField.getText());
				cList.add(contact);
				refresh(cList);
				frame.dispose();
					}
		});
		
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
	}
	
	
	@SuppressWarnings("static-access")
	public void editContact() {
		if ( table == null ||table.getSelectedRow() == -1){
			JOptionPane.showMessageDialog(null, "Please select the row for editing!");
		}
		else{
			
		textFields = new ArrayList<JTextField>();
		//Main.cList
		final JFrame frame = new JFrame();
		frame.setBounds(100, 100, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFirstName.setBounds(10, 11, 80, 17);
		frame.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLastName.setBounds(10, 36, 80, 17);
		frame.getContentPane().add(lblLastName);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress.setBounds(10, 61, 80, 17);
		frame.getContentPane().add(lblAddress);
		
		JLabel lblAddress_1 = new JLabel("Address 2:");
		lblAddress_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAddress_1.setBounds(10, 86, 80, 17);
		frame.getContentPane().add(lblAddress_1);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCity.setBounds(10, 111, 80, 17);
		frame.getContentPane().add(lblCity);
		
		JLabel lblState = new JLabel("State:");
		lblState.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblState.setBounds(10, 139, 80, 17);
		frame.getContentPane().add(lblState);
		
		JLabel lblZipcode = new JLabel("Zipcode:");
		lblZipcode.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblZipcode.setBounds(10, 167, 80, 17);
		frame.getContentPane().add(lblZipcode);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPhone.setBounds(10, 195, 80, 17);
		frame.getContentPane().add(lblPhone);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(10, 223, 80, 14);
		frame.getContentPane().add(lblEmail);
		
		final JTextField firstNameField = new JTextField();
		firstNameField.setBounds(100, 11, 230, 20);
		frame.getContentPane().add(firstNameField);
		firstNameField.setColumns(10);
		textFields.add(firstNameField);
		
		final JTextField lastNameField = new JTextField();
		lastNameField.setBounds(100, 36, 230, 20);
		frame.getContentPane().add(lastNameField);
		lastNameField.setColumns(10);
		textFields.add(lastNameField);
		
		final JTextField addressField = new JTextField();
		addressField.setBounds(100, 61, 230, 20);
		frame.getContentPane().add(addressField);
		addressField.setColumns(10);
		textFields.add(addressField);
		
		final JTextField address2Field = new JTextField();
		address2Field.setBounds(100, 86, 230, 20);
		frame.getContentPane().add(address2Field);
		address2Field.setColumns(10);
		textFields.add(address2Field);
		
		final JTextField cityField = new JTextField();
		cityField.setBounds(100, 111, 230, 20);
		frame.getContentPane().add(cityField);
		cityField.setColumns(10);
		textFields.add(cityField);
		
		final JTextField stateField = new JTextField();
		stateField.setBounds(100, 139, 230, 20);
		frame.getContentPane().add(stateField);
		stateField.setColumns(10);
		textFields.add(stateField);
		
		final JTextField zipcodeField = new JTextField();
		zipcodeField.setBounds(100, 167, 230, 20);
		frame.getContentPane().add(zipcodeField);
		zipcodeField.setColumns(10);
		textFields.add(zipcodeField);
		
		final JTextField phoneField = new JTextField();
		phoneField.setBounds(100, 195, 230, 20);
		frame.getContentPane().add(phoneField);
		phoneField.setColumns(10);
		textFields.add(phoneField);
		
		final JTextField emailField = new JTextField();
		emailField.setBounds(100, 222, 230, 20);
		frame.getContentPane().add(emailField);
		emailField.setColumns(10);
		textFields.add(emailField);
		
		JButton btnUpdateContact = new JButton("UPDATE CONTACT");
		btnUpdateContact.setBounds(371, 221, 178, 23);
		frame.getContentPane().add(btnUpdateContact);
		//if (table.getSelectedRow()==null)
		
			selectedRow = table.convertRowIndexToModel(table.getSelectedRow());
			for(int i = 0; i < textFields.size(); i++) {
					textFields.get(i).setText(table.getValueAt(selectedRow, i).toString());
			}
		
		
		btnUpdateContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cList.get(selectedRow).setFirstName(firstNameField.getText());
				cList.get(selectedRow).setLastName(lastNameField.getText());
				cList.get(selectedRow).setZipcode(zipcodeField.getText());
				cList.get(selectedRow).setAddress( addressField.getText());
				cList.get(selectedRow).setAddress2( address2Field.getText());
				cList.get(selectedRow).setPhone(phoneField.getText());
				cList.get(selectedRow).setCity(cityField.getText());
				cList.get(selectedRow).setState(stateField.getText());
				cList.get(selectedRow).setEmail(emailField.getText());
				
				refresh(cList);
				frame.dispose();
					}
		});
		
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
	}
}
	public void refresh(ArrayList<Contacts> clist) {
		
			if( clist.size() <= 1&&firstTime ) {
				buildTable();
			}
			else{

			 tableModel.setDataVector(Contacts.toStringsMatrix(clist), columns.toArray());}
		}
		
		

	public void deleteRefresh(ArrayList<Contacts> cList) {
		if(cList.size() <1 ) {
			 DefaultTableModel model = (DefaultTableModel) table.getModel();
			 model.setRowCount(0);
			 return;
		}
		 tableModel.setDataVector(Contacts.toStringsMatrix(cList), columns.toArray());
		 
	}
	 /*This is where the table gets built, anything for filling the table needs to go here
     * I've included code that demonstrates reading in from a text file and displaying it
     * on screen.*/
	public void buildTable() {
		        firstTime = false;
		        tableModel = new DefaultTableModel(Contacts.toStringsMatrix(cList), columns.toArray());
		        table = new JTable(tableModel);
		        JScrollPane tableContainer = new JScrollPane(table);
		        tableContainer.setBounds(10, 36, 833, 219);
		        addressBook_mainPanel.add(tableContainer);
		        //table.isCellEditable(1, 0);
		        table.addMouseListener(new MouseAdapter() {
		        	public void mouseClicked(MouseEvent e) {
		        		int selectedRow = table.convertRowIndexToModel(table.getSelectedRow());
		        		contactName.setText(cList.get(selectedRow).getFirstName() + " " + cList.get(selectedRow).getLastName());
		        		contactAddress.setText(cList.get(selectedRow).getAddress());
		        		contactAddress2.setText(cList.get(selectedRow).getAddress2());
		        		contactCity.setText(cList.get(selectedRow).getCity());
		        		contactState.setText(cList.get(selectedRow).getState());
		        		contactZip.setText(cList.get(selectedRow).getZipcode());
		        		contactPhone.setText(cList.get(selectedRow).getPhone());
		        		contactEmail.setText(cList.get(selectedRow).getEmail());
		        	}
		        });
		        }

	
	public void chooseAddressBook() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 517, 161);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblCreateNewAddress = new JLabel("Save As A New File:");
		lblCreateNewAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCreateNewAddress.setBounds(10, 72, 170, 14);
		frame.getContentPane().add(lblCreateNewAddress);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 97, 46, 14);
		frame.getContentPane().add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(66, 96, 231, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnCreateNew = new JButton("Save");
		btnCreateNew.setBounds(306, 95, 89, 23);
		frame.getContentPane().add(btnCreateNew);
		frame.setVisible(true);
		

	btnCreateNew.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String fs = textField_1.getText() + ".txt";
			File file = new File(System.getProperty("user.dir"), fs);
			writeFile(cList, file);
			frame.dispose();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}	
	});
}
	
	
public void openAddressBook() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 517, 161);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JLabel lblChooseAndAddress = new JLabel("Choose an Address Book:");
		lblChooseAndAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblChooseAndAddress.setBounds(10, 10, 170, 20);
		frame.getContentPane().add(lblChooseAndAddress);
		
		textField = new JTextField();
		textField.setBounds(10, 33, 188, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.setBounds(208, 32, 89, 23);
		frame.getContentPane().add(btnBrowse);
		
		JButton btnOpen = new JButton("Open");
		btnOpen.setBounds(306, 32, 89, 23);
		frame.getContentPane().add(btnOpen);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
		
	btnBrowse.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String OS = System.getProperty("os.name").toLowerCase();
			if(OS.indexOf("win") >= 0) {
				File startFile = new File(System.getProperty("user.dir"));
				JFileChooser fc = new JFileChooser(startFile);

				int returnVal = fc.showOpenDialog(frame); 

				File file = null;
				if (returnVal == JFileChooser.APPROVE_OPTION) {
				    file = fc.getSelectedFile();
				    textField.setText(file.getAbsolutePath());
				} else {
				    
				}
			} 
			else if (OS.indexOf("mac") >= 0) {
				
			}
		}
	});


	btnOpen.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String fs = textField.getText();
			importFromFile(cList, fs);
			//System.out.print(cList.size());
			refresh(cList);
			frame.dispose();
			
		}	
	});	
	}

	public void writeFile(ArrayList<Contacts> cl, File f) {
		// before we open the file check to see if it already exists
		boolean alreadyExists = f.exists();
		try {
			// use FileWriter constructor that specifies open for appending
			FileWriter fw = new FileWriter(f, true);
			PrintWriter outputFile = new PrintWriter(fw);
			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists)
			{
				outputFile.println("City	State	Zip Code	Address	Address2	First Name	Last Name	"
						+ "Phone Number	Email");
			}
			if (cl != null){
				for (int i = 0; i< cl.size(); i++){
				outputFile.println(cl.get(i).getCity()+"	"+cl.get(i).getState()+"	"+
						cl.get(i).getZipcode()+"	"+cl.get(i).getAddress()
						+"	"+cl.get(i).getAddress2()+"	"+cl.get(i).getFirstName()+"	"+cl.get(i).getLastName()
						+"	"+cl.get(i).getPhone()+"	"+cl.get(i).getEmail());
				}
			}
			outputFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
public void exportAddressBook() {
		frame = new JFrame();
		frame.setBounds(100, 100, 517, 161);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JLabel lblChooseAndAddress = new JLabel("Export to:");
		lblChooseAndAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblChooseAndAddress.setBounds(10, 10, 170, 20);
		frame.getContentPane().add(lblChooseAndAddress);
		
		textField_export = new JTextField();
		textField_export.setBounds(10, 33, 188, 20);
		frame.getContentPane().add(textField_export);
		textField_export.setColumns(10);
		
		JButton btnBrowse = new JButton("Specify a location:");
		btnBrowse.setBounds(208, 32, 89, 23);
		frame.getContentPane().add(btnBrowse);
		
		JButton btnExport = new JButton("Export!");
		btnExport.setBounds(306, 32, 89, 23);
		frame.getContentPane().add(btnExport);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
		
	btnBrowse.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String OS = System.getProperty("os.name").toLowerCase();
			if(OS.indexOf("win") >= 0) {
				File startFile = new File(System.getProperty("user.dir"));
				JFileChooser fc = new JFileChooser(startFile);

				int returnVal = fc.showOpenDialog(frame); 

				File file = null;
				if (returnVal == JFileChooser.APPROVE_OPTION) {
				    file = fc.getSelectedFile();
				    textField_export.setText(file.getAbsolutePath());
				} else {
				    
				}
			} 
			else if (OS.indexOf("mac") >= 0) {
				
			}
		}
	});


	btnExport.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String fs = textField_export.getText();
			fileName = fs + ".tsv"; 
			File file = new File(fileName);
			writeFile(cList, file);
			frame.dispose();
		}	
	});	
	}
	
	
	
	
	public void saveFile(int count){
		fileName = "File"+Integer.toString(count)+".txt"; 
		File file = new File(System.getProperty("user.dir"), fileName);
		//System.out.print(textField_1.getText());
		if (!file.exists()){
			overwriteFile(cList, file);
		}
		
		else{
			counter = count +1;
			saveFile(counter);
		}
	}
	
	public void overwriteFile(ArrayList<Contacts> cl, File f) {
		// before we open the file check to see if it already exists
		//boolean alreadyExists = f.exists();
		try {
			// use FileWriter constructor that specifies open for appending
			FileWriter fw = new FileWriter(f);
			PrintWriter outputFile = new PrintWriter(fw);
			// if the file didn't already exist then we need to write out the header line
			//if (!alreadyExists)
			//{
				outputFile.println("City	State	Zip Code	Address	Address2	First Name	Last Name	"
						+ "Phone Number	Email");
			//}
			if (cl != null){
				for (int i = 0; i< cl.size(); i++){
				outputFile.println(cl.get(i).getCity()+"	"+cl.get(i).getState()+"	"+
						cl.get(i).getZipcode()+"	"+cl.get(i).getAddress()
						+"	"+cl.get(i).getAddress2()+"	"+cl.get(i).getFirstName()+"	"+cl.get(i).getLastName()
						+"	"+cl.get(i).getPhone()+"	"+cl.get(i).getEmail());
				}
			}
			outputFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void setDefaultCloseOperation(int EXIT_ON_CLOSE){
						
	
					
				}
	public void importFromFile(ArrayList<Contacts> cl, String path){
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            String headerLine = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] temp = line.split("\\t");

                Contacts contact = new Contacts();
        		contact.setFirstName(temp[0]);
        		contact.setLastName(temp[1]);
        		contact.setAddress(temp[2]);
        		contact.setZipcode(temp[3]);
        		contact.setAddress2(temp[4]);
        		contact.setPhone(temp[5]);
        		contact.setCity(temp[6]);
        		contact.setState(temp[7]);
        		contact.setEmail(temp[8]);
                cl.add(contact);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public ArrayList<Contacts>  sortName( ArrayList<Contacts> contactList){
		 //ArrayList<Contacts> contacts = new ArrayList<Contacts>();
		 Collections.sort(contactList, new Comparator<Contacts>(){
			 @Override
			 public int compare(Contacts c1, Contacts c2){
				 
			    int nameCmp = (c1.getFirstName()+c1.getLastName()).compareTo(c2.getFirstName()+c2.getLastName());
			    /*
			    if (fnameCmp != 0) {
			        return fnameCmp;
			    }
			    int lnameCmp = c1.getLastName().compareTo(c2.getLastName());
			    if (lnameCmp != 0) {
			        return lnameCmp;
			        */
			    return nameCmp; 
			    }
			    
				
	
			    //return c1.getZipcode().compareTo(c2.getZipcode());
			
	});
		 return contactList;
	
	
	}
	
	
	public ArrayList<Contacts>  sortZipcode(  ArrayList<Contacts> contactList){
		 //ArrayList<Contacts> contacts = new ArrayList<Contacts>();
		final ArrayList<Contacts> contList = contactList;
		 Collections.sort(contactList, new Comparator<Contacts>(){
			 @Override
			 public int compare(Contacts c1, Contacts c2){
				 
				 int zipcodeCmp = c1.getZipcode().compareTo(c2.getZipcode());
				 int nameCmp = (c1.getFirstName()+c1.getLastName()).compareTo(c2.getFirstName()+c2.getLastName());
				 if (zipcodeCmp == 0) {
					 return nameCmp;
				 }
				return zipcodeCmp;
	
			}
	});
		 return contactList;
	
	
	}


       
    }

